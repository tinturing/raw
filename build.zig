const std = @import("std");

pub fn build(b: *std.build.Builder) void {
    const target = b.standardTargetOptions(.{});
    const mode = b.standardReleaseOptions();

    const lib = b.addStaticLibrary("raw", "raw.zig");
    lib.setBuildMode(mode);
    lib.install();

    const pkg = std.build.Pkg{
        .name = "raw",
        .path = .{ .path = "raw.zig" },
    };

    {
        const exe = b.addExecutable("loop", "examples/loop.zig");
        exe.setTarget(target);
        exe.setBuildMode(mode);
        exe.install();

        // TODO Why doesn't this work?
        // exe.linkLibrary(lib);

        exe.addPackage(pkg);

        const cmd = exe.run();
        cmd.step.dependOn(b.getInstallStep());
        if (b.args) |args| {
            cmd.addArgs(args);
        }

        const run_step = b.step("loop", "Run the loop example");
        run_step.dependOn(&cmd.step);
    }
}
