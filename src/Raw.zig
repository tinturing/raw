const std = @import("std");
const os = std.os;
const linux = os.linux;
const Raw = @This();

file: std.fs.File,
original: os.termios,

// TODO Should this take a std.fs.File or just a std.os.fd_t?
pub fn init(file: std.fs.File) !Raw {
    var termios = try os.tcgetattr(file.handle);
    const original = termios;

    termios.iflag &= ~@as(c_uint, linux.BRKINT | linux.ICRNL | linux.INPCK | linux.ISTRIP | linux.IXON);
    termios.lflag &= ~@as(c_uint, linux.ECHO | linux.ICANON | linux.IEXTEN | linux.ISIG);
    termios.cc[linux.V.MIN] = 0;
    termios.cc[linux.V.TIME] = 0;

    try os.tcsetattr(file.handle, os.TCSA.FLUSH, termios);

    return Raw{
        .file = file,
        .original = original,
    };
}

pub fn deinit(self: *const Raw) void {
    // TODO How can this error be handled?
    os.tcsetattr(self.file.handle, os.TCSA.FLUSH, self.original) catch {};
}
