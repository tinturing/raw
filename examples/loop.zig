const std = @import("std");
const raw = @import("raw");

const stdout = std.io.getStdOut();
const stdin = std.io.getStdIn();

const max_events = 32;
const fps = 5;
const sleep_time = (std.time.ms_per_s / fps) * std.time.ns_per_ms;

pub fn main() anyerror!void {
    const rawFile = try raw.Raw.init(stdin);
    defer rawFile.deinit();

    var buf: [max_events]u8 = undefined;

    try stdout.writeAll("Press q or ^C to quit.\n");

    var frames: usize = 0;
    loop: while (true) : (frames += 1) {
        const count = try stdin.read(buf[0..]);
        try stdout.writer().print("{any}\n", .{buf[0..count]});

        switch (buf[0]) {
            3, 'q' => break :loop,
            else => {}, // ...
        }

        std.time.sleep(sleep_time);
    }
}
