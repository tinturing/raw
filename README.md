# raw

A library for uncooking Linux terminals.
Written in [Zig](https://ziglang.org).

## Example

Run the example:

```sh
zig build loop
```
